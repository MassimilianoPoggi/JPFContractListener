Build: [![pipeline status](https://gitlab.com/MassimilianoPoggi/JPFContractListener/badges/master/pipeline.svg)](https://gitlab.com/MassimilianoPoggi/JPFContractListener/commits/master)

Adaptation of [jpf-aprop/contract](https://babelfish.arc.nasa.gov/trac/jpf/wiki/projects/jpf-aprop/contract) (with a few extensions) for the current version of JPF.