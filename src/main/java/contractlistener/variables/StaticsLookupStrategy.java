package contractlistener.variables;

import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;

public class StaticsLookupStrategy implements VariablesLookupStrategy {

    private ClassInfo classInfo;

    public StaticsLookupStrategy(ClassInfo classInfo) {
        this.classInfo = classInfo;
    }

    private ElementInfo getStaticElementInfo(String key) {
        for (ElementInfo info : classInfo.getStatics())
            if (info.getClassInfo().getSimpleName().equals(key))
                return info;

        return null;
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        ElementInfo staticElement = getStaticElementInfo(key);
        if (staticElement != null)
            return staticElement;

        throw new IllegalArgumentException("Couldn't find imported static element " + key);
    }

    @Override
    public boolean hasValue(String key) {
        return getStaticElementInfo(key) != null;
    }
}
