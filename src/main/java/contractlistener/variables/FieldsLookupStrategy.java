package contractlistener.variables;

import gov.nasa.jpf.vm.*;

public class FieldsLookupStrategy implements VariablesLookupStrategy {
    private ClassInfo classInfo;
    private ElementInfo thisElement;
    private StaticsLookupStrategy staticsLookupStrategy;

    public FieldsLookupStrategy(ClassInfo classInfo) {
        this.classInfo = classInfo;
        this.staticsLookupStrategy = new StaticsLookupStrategy(classInfo);
    }

    public FieldsLookupStrategy(ElementInfo thisElement) {
        this(thisElement.getClassInfo());
        this.thisElement = thisElement;
    }

    public FieldsLookupStrategy(ThreadInfo currentThread) {
        this(currentThread.getExecutingClassInfo());

        if (!currentThread.getTopFrameMethodInfo().isStatic())
            this.thisElement = currentThread.getThisElementInfo();
    }

    private FieldInfo getStaticField(String key) {
        return classInfo.getStaticField(key);
    }

    private FieldInfo getInstanceField(String key) {
        if (thisElement != null)
            return classInfo.getInstanceField(key);

        return null;
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        FieldInfo staticField = getStaticField(key);
        if (staticField != null)
            return classInfo.getStaticFieldValueObject(key);

        FieldInfo instanceField = getInstanceField(key);
        if (instanceField != null)
            return thisElement.getFieldValueObject(key);

        if (staticsLookupStrategy.hasValue(key))
            return staticsLookupStrategy.getValue(key);

        throw new IllegalArgumentException("Field " + key + " not found.");
    }

    @Override
    public boolean hasValue(String key) {
        return getStaticField(key) != null || getInstanceField(key) != null || staticsLookupStrategy.hasValue(key);
    }
}
