package contractlistener.variables;

public interface VariablesLookupStrategy {
    Object getValue(String key) throws IllegalArgumentException;
    boolean hasValue(String key);
}
