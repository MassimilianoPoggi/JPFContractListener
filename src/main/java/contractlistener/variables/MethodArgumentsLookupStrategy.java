package contractlistener.variables;

import gov.nasa.jpf.vm.LocalVarInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.Arrays;

public class MethodArgumentsLookupStrategy implements VariablesLookupStrategy {
    private ThreadInfo currentThread;

    public MethodArgumentsLookupStrategy(ThreadInfo currentThread) {
        this.currentThread = currentThread;
    }

    private LocalVarInfo getLocalVar(String key) {
        LocalVarInfo[] localVars = currentThread.getTopFrameMethodInfo().getArgumentLocalVars();
        if (localVars == null)
            return null;

        // manual search because the lookup methods don't work?!
        for (LocalVarInfo info : localVars)
            if (info.getName().equals(key))
                return info;

        return null;
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        LocalVarInfo localVar = getLocalVar(key);
        if (localVar != null)
            return currentThread.getTopFrame().getLocalValueObject(getLocalVar(key));

        throw new IllegalArgumentException("Argument " + key + " not found.");
    }

    @Override
    public boolean hasValue(String key) {
        return getLocalVar(key) != null;
    }
}
