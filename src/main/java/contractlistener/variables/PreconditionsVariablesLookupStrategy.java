package contractlistener.variables;

import gov.nasa.jpf.vm.ThreadInfo;
import contractlistener.contracts.parser.ast.Variable;

public class PreconditionsVariablesLookupStrategy implements VariablesLookupStrategy {
    private VariablesLookupStrategy fieldsLookupStrategy;
    private VariablesLookupStrategy argumentsLookupStrategy;

    public PreconditionsVariablesLookupStrategy(ThreadInfo currentThread) {
        this.fieldsLookupStrategy = new FieldsLookupStrategy(currentThread);
        this.argumentsLookupStrategy = new MethodArgumentsLookupStrategy(currentThread);
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        // arguments have priority over fields
        if (argumentsLookupStrategy.hasValue(key))
            return argumentsLookupStrategy.getValue(key);

        if (fieldsLookupStrategy.hasValue(key))
            return fieldsLookupStrategy.getValue(key);

        throw new IllegalArgumentException("Precondition variable " + key + " not found.");
    }

    @Override
    public boolean hasValue(String key) {
        return fieldsLookupStrategy.hasValue(key) || argumentsLookupStrategy.hasValue(key);
    }
}
