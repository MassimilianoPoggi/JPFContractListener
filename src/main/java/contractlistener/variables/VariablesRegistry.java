package contractlistener.variables;

import java.util.HashMap;
import java.util.Map;

public class VariablesRegistry {
    private VariablesLookupStrategy variablesLookupStrategy;
    private Map<Object, Object> savedValues;

    public VariablesRegistry(VariablesLookupStrategy strategy) {
        this.savedValues = new HashMap<>();
        this.variablesLookupStrategy = strategy;
    }

    public Object getValue(Object key) {
        if (savedValues.containsKey(key))
            return savedValues.get(key);

        if (variablesLookupStrategy.hasValue(key.toString()))
            return variablesLookupStrategy.getValue(key.toString());

        throw new IllegalArgumentException("Registry couldn't find variable " + key.toString());
    }

    public void saveValue(Object key, Object value) {
        savedValues.put(key, value);
    }

    public boolean containsSavedValue(Object key) {
        return savedValues.containsKey(key);
    }

    public void setLookupStrategy(VariablesLookupStrategy lookupStrategy) {
        this.variablesLookupStrategy = lookupStrategy;
    }
}
