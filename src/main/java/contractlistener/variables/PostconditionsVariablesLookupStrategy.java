package contractlistener.variables;

import gov.nasa.jpf.jvm.bytecode.JVMReturnInstruction;
import gov.nasa.jpf.vm.ThreadInfo;
import contractlistener.contracts.parser.ast.Variable;

import java.util.Arrays;

public class PostconditionsVariablesLookupStrategy implements VariablesLookupStrategy {
    private ThreadInfo currentThread;
    private JVMReturnInstruction returnInstruction;
    private VariablesLookupStrategy preconditionsVariablesLookupStrategy;

    public PostconditionsVariablesLookupStrategy(ThreadInfo currentThread, JVMReturnInstruction returnInstruction) {
        this.preconditionsVariablesLookupStrategy = new PreconditionsVariablesLookupStrategy(currentThread);
        this.currentThread = currentThread;
        this.returnInstruction = returnInstruction;
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        if (key.equals(Variable.RESULT_NAME))
            return returnInstruction.getReturnValue(currentThread);

        if (preconditionsVariablesLookupStrategy.hasValue(key))
            return preconditionsVariablesLookupStrategy.getValue(key);

        throw new IllegalArgumentException("Postcondition variable " + key + " not found.");
    }

    @Override
    public boolean hasValue(String key) {
        return key.equals(Variable.RESULT_NAME) || preconditionsVariablesLookupStrategy.hasValue(key);
    }
}