package contractlistener.tools;

import gov.nasa.jpf.util.MethodSpec;
import gov.nasa.jpf.vm.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MethodCaller {
    private String methodName;
    private List<Object> arguments;
    private ThreadInfo executionThread;

    public MethodCaller(ThreadInfo executionThread, String methodName, List<Object> arguments) {
        this.methodName = methodName;
        this.arguments = arguments == null ? new ArrayList<>() : arguments;
        this.executionThread = executionThread;
    }

    private int getObjectReference(Object o) {
        if (o instanceof ElementInfo)
            return ((ElementInfo) o).getObjectRef();

        MJIEnv env = executionThread.getEnv();

        if (o == null)
            return MJIEnv.NULL;
        if (o instanceof Integer)
            return env.newInteger((Integer) o);
        if (o instanceof Long)
            return env.newLong((Long) o);
        if (o instanceof Boolean)
            return env.newBoolean((Boolean) o);
        if (o instanceof Double)
            return env.newDouble((Double) o);
        if (o instanceof Float)
            return env.newFloat((Float) o);
        if (o instanceof Character)
            return env.newCharacter((Character) o);
        if (o instanceof String)
            return env.newString(o.toString());

        throw new IllegalArgumentException("Unexpected argument type: " + o.getClass());
    }

    private MethodInfo getMethod(ClassInfo classInfo) {
        MethodSpec methodSpec = MethodSpec.createMethodSpec("*." + methodName);
        List<MethodInfo> methodInfos = classInfo.getMatchingMethodInfos(methodSpec);

        if (methodInfos == null) {
            ClassInfo superClassInfo = classInfo.getSuperClass();
            if (superClassInfo != null)
                return getMethod(superClassInfo);

            throw new IllegalArgumentException("Method " + methodName + " not found.");
        }

        methodInfos = methodInfos.stream()
                .filter((m) -> m.getNumberOfArguments() == arguments.size())
                .collect(Collectors.toList());

        // todo: filter by parameters (and their superclasses), resolve conflicts java-style
        // java finds definition ambiguous only if two methods can both accept the parameters
        // and one is not a subset of the other
        if (methodInfos.size() > 1)
            throw new IllegalArgumentException("Couldn't identify method: too many alternatives.");

        return methodInfos.get(0);
    }

    private ClassInfo getClassInfo(Object target) {
        if (isPrimitive(target) || target instanceof String)
            return ClassInfo.getInitializedClassInfo(target.getClass().getName(), executionThread);

        return ((ElementInfo) target).getClassInfo();
    }

    private boolean isPrimitive(Object o) {
        return o instanceof Number || o instanceof Boolean || o instanceof Character;
    }

    public Object invoke(Object target) {
        Memento<ThreadInfo> memento = executionThread.getMemento();

        MethodInfo method = getMethod(getClassInfo(target));

        StackFrame frame = method.createDirectCallStackFrame(executionThread, 0);
        if (!method.isStatic())
            frame.push(getObjectReference(target));

        for (int i = 0; i < arguments.size(); i++)
            pushArgumentValue(method, frame, i);

        executionThread.executeMethodHidden(frame);

        memento.restore(executionThread);

        return getResultObject(method, frame);
    }

    private void pushArgumentValue(MethodInfo method, StackFrame frame, int i) {
        Object argument = arguments.get(i);

        switch(method.getArgumentTypeNames()[i]) {
            case "byte":
            case "short":
            case "int":
                if (argument instanceof Integer)
                    frame.push((Integer) argument);
                else
                    frame.push(getValue((ElementInfo) argument));
                break;
            case "float":
                if (argument instanceof Float)
                    frame.pushFloat((Float) argument);
                else
                    frame.pushFloat(getValue((ElementInfo) argument));
                break;
            case "long":
                if (argument instanceof Long)
                    frame.pushLong((Long) argument);
                else
                    frame.pushLong(getValue((ElementInfo) argument));
                break;
            case "double":
                if (argument instanceof Double)
                    frame.pushDouble((Double) argument);
                else
                    frame.pushDouble(getValue((ElementInfo) argument));
                break;
            case "boolean":
                if (argument instanceof Boolean)
                    frame.push((Boolean) argument ? 1 : 0);
                else
                    frame.push(getValue((ElementInfo) argument) ? 1 : 0);
                break;
            default:
                frame.pushRef(getObjectReference(arguments.get(i)));
                break;
        }
    }

    private <T> T getValue(ElementInfo argument) {
        return (T) argument.getFieldValueObject("value");
    }

    private Object getResultObject(MethodInfo method, StackFrame frame) {
        if (!method.isReferenceReturnType())
            switch(method.getReturnTypeName()) {
                case "byte":
                case "short":
                case "int":
                    return frame.getResult();
                case "float":
                    return frame.getFloatResult();
                case "long":
                    return frame.getLongResult();
                case "double":
                    return frame.getDoubleResult();
                case "boolean":
                    return frame.getResult() != 0;
                case "char":
                    return Character.toChars(frame.getResult())[0];
                default:
                    throw new IllegalArgumentException("Primitive type " + method.getReturnTypeName() + " not found.");
            }

        int reference = frame.getReferenceResult();
        if (reference != MJIEnv.NULL)
            return executionThread.getMJIEnv().getElementInfo(reference);

        return null;
    }
}