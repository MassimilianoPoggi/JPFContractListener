package contractlistener;

import contractlistener.annotations.Ensures;
import contractlistener.annotations.InstanceInvariant;
import contractlistener.annotations.Requires;
import contractlistener.annotations.StaticInvariant;
import contractlistener.contracts.*;
import contractlistener.contracts.composition.ContractAnd;
import contractlistener.contracts.inheritance.InheritanceCompositionStrategy;
import contractlistener.contracts.parser.ContractSpecLexer;
import contractlistener.contracts.parser.ContractSpecParser;
import contractlistener.contracts.wrappers.InstanceInvariantsContract;
import contractlistener.contracts.wrappers.PostconditionsContract;
import contractlistener.contracts.wrappers.PreconditionsContract;
import contractlistener.contracts.wrappers.StaticInvariantsContract;
import gov.nasa.jpf.vm.AnnotationInfo;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.InfoObject;
import gov.nasa.jpf.vm.MethodInfo;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public class ContractLoader {
    private InheritanceCompositionStrategy inheritanceStrategy;

    public ContractLoader(InheritanceCompositionStrategy strategy) {
        this.inheritanceStrategy = strategy;
    }

    public Contract getInstanceInvariantsContract(ClassInfo cls) {
        if (cls.hasAttr(InstanceInvariantsContract.class))
            return cls.getAttr(InstanceInvariantsContract.class);

        Contract contract = loadContract(InstanceInvariant.class, cls);

        ClassInfo superCls = cls.getSuperClass();
        if (superCls != null)
            contract = inheritanceStrategy.composeInvariants(contract, getInstanceInvariantsContract(superCls));

        contract = new ContractAnd(contract, getStaticInvariantsContract(cls));
        contract = new InstanceInvariantsContract(contract);
        cls.addAttr(contract);

        return contract;
    }

    public Contract getStaticInvariantsContract(ClassInfo cls) {
        if (cls.hasAttr(StaticInvariantsContract.class))
            return cls.getAttr(StaticInvariantsContract.class);

        Contract contract = loadContract(StaticInvariant.class, cls);

        ClassInfo superCls = cls.getSuperClass();
        if (superCls != null)
            contract = inheritanceStrategy.composeInvariants(contract, getStaticInvariantsContract(superCls));

        contract = new StaticInvariantsContract(contract);
        cls.addAttr(contract);

        return contract;
    }

    public Contract getPreconditionsContract(MethodInfo method) {
        if (method.hasAttr(PreconditionsContract.class))
            return method.getAttr(PreconditionsContract.class);

        Contract contract = new PreconditionsContract(loadContract(Requires.class, method));

        MethodInfo overriddenMethod = method.getOverriddenMethodInfo();
        if (overriddenMethod != null)
            contract = inheritanceStrategy.composePreconditions(contract, getPreconditionsContract(overriddenMethod));

        contract = new PreconditionsContract(contract);
        method.addAttr(contract);

        return contract;
    }

    public Contract getPostconditionsContract(MethodInfo method) {
        if (method.hasAttr(PostconditionsContract.class))
            return method.getAttr(PostconditionsContract.class);

        Contract contract = loadContract(Ensures.class, method);

        MethodInfo overriddenMethod = method.getOverriddenMethodInfo();
        if (overriddenMethod != null)
            contract = inheritanceStrategy.composePostconditions(contract, getPostconditionsContract(overriddenMethod));

        contract = new PostconditionsContract(contract);
        method.addAttr(contract);

        return contract;
    }

    private Contract loadContract(Class<?> annotation, InfoObject object) {
        Contract contract = new EmptyContract();

        if (!object.hasAnnotation(annotation.getName()))
            return contract;

        AnnotationInfo annotationInfo = object.getAnnotation(annotation.getName());

        for (String constraint : annotationInfo.getValueAsStringArray()) {
            Contract clause = parseContract(constraint);
            contract = new ContractAnd(contract, clause);
        }

        return contract;
    }

    private Contract parseContract(String constraint) {
        CharStream charStream = CharStreams.fromString(constraint);
        ContractSpecLexer lexer = new ContractSpecLexer(charStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);
        ContractSpecParser parser = new ContractSpecParser(tokenStream);

        return new SimpleContract(parser.booleanExpr().o);
    }
}
