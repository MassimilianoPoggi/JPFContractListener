package contractlistener.contracts.wrappers;

import contractlistener.contracts.Contract;
import contractlistener.variables.VariablesRegistry;

public class InstanceInvariantsContract implements Contract {
    private Contract c;

    public InstanceInvariantsContract(Contract c) {
        this.c = c;
    }

    @Override
    public boolean isSatisfied(VariablesRegistry registry) {
        return c.isSatisfied(registry);
    }

    @Override
    public void saveOldValues(VariablesRegistry registry) {
        c.saveOldValues(registry);
    }

    @Override
    public String toString() {
        return c.toString();
    }
}