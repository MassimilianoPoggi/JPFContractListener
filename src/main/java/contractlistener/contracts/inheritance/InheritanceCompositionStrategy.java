package contractlistener.contracts.inheritance;

import contractlistener.contracts.Contract;

public interface InheritanceCompositionStrategy {
    Contract composePreconditions(Contract c1, Contract c2);
    Contract composePostconditions(Contract c1, Contract c2);
    Contract composeInvariants(Contract c1, Contract c2);
}
