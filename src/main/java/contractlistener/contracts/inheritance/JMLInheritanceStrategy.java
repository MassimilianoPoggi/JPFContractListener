package contractlistener.contracts.inheritance;

import contractlistener.contracts.Contract;
import contractlistener.contracts.composition.ContractAnd;
import contractlistener.contracts.composition.ContractOr;

public class JMLInheritanceStrategy implements InheritanceCompositionStrategy {
    @Override
    public Contract composePreconditions(Contract c1, Contract c2) {
        return new ContractAnd(c1, c2);
    }

    @Override
    public Contract composePostconditions(Contract c1, Contract c2) {
        return new ContractAnd(c1, c2);
    }

    @Override
    public Contract composeInvariants(Contract c1, Contract c2) {
        return new ContractAnd(c1, c2);
    }
}
