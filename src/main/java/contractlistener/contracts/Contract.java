package contractlistener.contracts;

import contractlistener.variables.VariablesRegistry;

public interface Contract {
    boolean isSatisfied(VariablesRegistry registry);
    void saveOldValues(VariablesRegistry registry);
}
