package contractlistener.contracts;

import contractlistener.variables.VariablesRegistry;

public class EmptyContract implements Contract {
    @Override
    public boolean isSatisfied(VariablesRegistry registry) {
        return true;
    }

    @Override
    public void saveOldValues(VariablesRegistry registry) {

    }

    @Override
    public String toString() {
        return "true";
    }
}
