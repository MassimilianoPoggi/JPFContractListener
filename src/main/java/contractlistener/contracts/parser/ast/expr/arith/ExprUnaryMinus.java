package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;

public class ExprUnaryMinus extends ArithmeticUnaryExpr {
    public ExprUnaryMinus(Operand o) {
        super(o);
    }

    @Override
    Integer integerOperation(int n) {
        return -n;
    }

    @Override
    Double floatingPointOperation(double d) {
        return -d;
    }

    @Override
    String operatorString() {
        return "-";
    }
}
