package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public abstract class ArithmeticUnaryExpr implements Operand {
    private Operand o;

    public ArithmeticUnaryExpr(Operand o) {
        this.o = o;
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        o.saveOldValue(registry);
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        Number n = o.getNumberValue(registry);

        if (hasFloatingPointValue(n))
            return floatingPointOperation(n.doubleValue());

        return integerOperation(n.intValue());
    }

    protected boolean hasFloatingPointValue(Number n) {
        return n instanceof Double || n instanceof Float;
    }

    abstract Integer integerOperation(int n);
    abstract Double floatingPointOperation(double d);

    @Override
    public String toString() {
        return operatorString() + " " + o.toString();
    }

    abstract String operatorString();
}
