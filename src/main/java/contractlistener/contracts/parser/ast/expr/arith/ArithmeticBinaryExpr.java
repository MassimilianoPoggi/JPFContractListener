package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public abstract class ArithmeticBinaryExpr implements Operand {
    private Operand o1;
    private Operand o2;

    public ArithmeticBinaryExpr(Operand o1, Operand o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        o1.saveOldValue(registry);
        o2.saveOldValue(registry);
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        Number a = o1.getNumberValue(registry);
        Number b = o2.getNumberValue(registry);

        if (hasFloatingPointValue(a, b))
            return floatingPointOperation(a.doubleValue(), b.doubleValue());

        return integerOperation(a.intValue(), b.intValue());
    }

    protected boolean hasFloatingPointValue(Number o1, Number o2) {
        return o1 instanceof Double || o1 instanceof Float || o2 instanceof Double || o2 instanceof Float;
    }

    abstract Integer integerOperation(int n1, int n2);
    abstract Double floatingPointOperation(double d1, double d2);

    @Override
    public String toString() {
        return o1.toString() + " " + operatorString() + " " + o2.toString();
    }

    abstract String operatorString();
}
