package contractlistener.contracts.parser.ast.expr.bool.comparators;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public class ComparatorGE extends BooleanComparator {
    public ComparatorGE(Operand o1, Operand o2) {
        super(o1, o2);
    }

    @Override
    protected String comparatorString() {
        return ">=";
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        return compareNumericValues(registry) >= 0;
    }
}
