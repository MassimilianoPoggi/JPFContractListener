package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;

public class ExprUnaryPlus extends ArithmeticUnaryExpr {
    public ExprUnaryPlus(Operand o) {
        super(o);
    }

    @Override
    Integer integerOperation(int n) {
        return n;
    }

    @Override
    Double floatingPointOperation(double d) {
        return d;
    }

    @Override
    String operatorString() {
        return "+";
    }
}
