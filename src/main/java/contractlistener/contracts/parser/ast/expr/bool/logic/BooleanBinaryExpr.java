package contractlistener.contracts.parser.ast.expr.bool.logic;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;
import gov.nasa.jpf.vm.ElementInfo;

public abstract class BooleanBinaryExpr implements Operand {
    private Operand o1;
    private Operand o2;

    public BooleanBinaryExpr(Operand o1, Operand o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        o1.saveOldValue(registry);
        o2.saveOldValue(registry);
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        boolean a = getBooleanValue(o1, registry);
        boolean b = getBooleanValue(o2, registry);

        return operation(a, b);
    }

    private boolean getBooleanValue(Operand o, VariablesRegistry registry) {
        Object val = o.getValue(registry);

        if (val instanceof Boolean)
            return (Boolean) val;

        if (val instanceof ElementInfo && ((ElementInfo)val).getFieldValueObject("value") instanceof Boolean)
            return (Boolean) ((ElementInfo) val).getFieldValueObject("value");

        throw new IllegalArgumentException(o + " is not a boolean.");
    }

    abstract boolean operation(boolean b1, boolean b2);

    @Override
    public String toString() {
        return o1.toString() + " " + operatorString() + " " + o2.toString();
    }

    abstract String operatorString();
}
