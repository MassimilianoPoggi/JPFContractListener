package contractlistener.contracts.parser.ast.expr.bool.comparators;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public abstract class BooleanComparator implements Operand {
    protected Operand o1;
    protected Operand o2;

    public BooleanComparator(Operand o1, Operand o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    protected int compareNumericValues(VariablesRegistry registry) {
        Number a = o1.getNumberValue(registry);
        Number b = o2.getNumberValue(registry);

        if (a instanceof Float || a instanceof Double || b instanceof Float || b instanceof Double)
            return (int) Math.signum(a.doubleValue() - b.doubleValue());

        return a.intValue() - b.intValue();
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        o1.saveOldValue(registry);
        o2.saveOldValue(registry);
    }

    @Override
    public String toString() {
        return o1.toString() + " " + comparatorString() + " " + o2.toString();
    }

    protected abstract String comparatorString();
}