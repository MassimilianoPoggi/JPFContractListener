package contractlistener.contracts.parser.ast.expr.bool.comparators;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public class ComparatorEQ extends BooleanComparator {
    public ComparatorEQ(Operand o1, Operand o2) {
        super(o1, o2);
    }

    @Override
    protected String comparatorString() {
        return "==";
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        if (o1.isNumberValue(registry) && o2.isNumberValue(registry))
            return compareNumericValues(registry) == 0;

        Object o1Val = o1.getValue(registry);
        Object o2Val = o2.getValue(registry);

        if (o1Val == null && o2Val == null)
            return true;
        if (o1Val == null || o2Val == null)
            return false;

        return o1Val.equals(o2Val);
    }
}
