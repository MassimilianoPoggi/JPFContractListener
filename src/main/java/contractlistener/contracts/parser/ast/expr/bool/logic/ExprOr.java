package contractlistener.contracts.parser.ast.expr.bool.logic;

import contractlistener.contracts.parser.ast.Operand;

public class ExprOr extends BooleanBinaryExpr {
    public ExprOr(Operand o1, Operand o2) {
        super(o1, o2);
    }

    @Override
    boolean operation(boolean b1, boolean b2) {
        return b1 || b2;
    }

    @Override
    String operatorString() {
        return "||";
    }
}
