package contractlistener.contracts.parser.ast.expr.lang;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.tools.MethodCaller;
import contractlistener.variables.VariablesRegistry;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MethodCall implements Operand {
    private Operand target;
    private String method;
    private List<Operand> arguments;

    public MethodCall(Operand target, String method) {
        this.target = target;
        this.method = method;
        arguments = new ArrayList<>();
    }

    public void addArgument(Operand o) {
        this.arguments.add(o);
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        List<Object> argumentValues = new ArrayList<>();
        for (Operand o : arguments)
            argumentValues.add(o.getValue(registry));

        Object targetValue = target.getValue(registry);

        if (targetValue == null)
            throw new IllegalArgumentException("Target for method invocation is null!");

        MethodCaller caller = new MethodCaller(ThreadInfo.getCurrentThread(), method, argumentValues);

        return caller.invoke(targetValue);
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        for (Operand o: arguments)
            o.saveOldValue(registry);
    }

    @Override
    public String toString() {
        return target.toString() +
                "." + method
                + "("
                + arguments.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining(", "))
                + ")";
    }
}
