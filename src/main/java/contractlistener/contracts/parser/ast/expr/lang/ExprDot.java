package contractlistener.contracts.parser.ast.expr.lang;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.FieldsLookupStrategy;
import contractlistener.variables.VariablesRegistry;
import gov.nasa.jpf.vm.ElementInfo;

public class ExprDot implements Operand {
    private Operand object;
    private Operand field;

    public ExprDot(Operand object, Operand field) {
        this.object = object;
        this.field = field;
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        Object objectValue = object.getValue(registry);

        if (!(objectValue instanceof ElementInfo))
            throw new UnsupportedOperationException("Dot operator only works on objects.");

        return new FieldsLookupStrategy((ElementInfo)objectValue).getValue(field.toString());
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        object.saveOldValue(registry);
        field.saveOldValue(registry);
    }

    @Override
    public String toString() {
        return object.toString() + "." + field.toString();
    }
}
