package contractlistener.contracts.parser.ast.expr.bool.logic;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;
import gov.nasa.jpf.vm.ElementInfo;

public class ExprNot implements Operand {
    private Operand operand;

    public ExprNot(Operand operand) {
        this.operand = operand;
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        Object objectValue = operand.getValue(registry);

        if (objectValue instanceof Boolean)
            return !((Boolean) objectValue);

        if (objectValue instanceof ElementInfo
                && ((ElementInfo) objectValue).getFieldValueObject("value") instanceof Boolean)
            return !(Boolean) ((ElementInfo) objectValue).getFieldValueObject("value");

        throw new IllegalArgumentException("Object value is not an instance of Boolean.");
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        operand.saveOldValue(registry);
    }

    @Override
    public String toString() {
        return "!" + operand.toString();
    }
}
