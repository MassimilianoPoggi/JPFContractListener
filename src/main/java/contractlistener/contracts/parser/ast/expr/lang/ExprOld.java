package contractlistener.contracts.parser.ast.expr.lang;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public class ExprOld implements Operand {
    private Operand o;

    public ExprOld(Operand o) {
        this.o = o;
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        if (registry.containsSavedValue(this))
            return registry.getValue(this);

        throw new IllegalArgumentException("Old expression value not saved.");
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        if (!registry.containsSavedValue(this))
            registry.saveValue(this, o.getValue(registry));
    }

    @Override
    public String toString() {
        return "\\old(" + o.toString() + ")";
    }
}
