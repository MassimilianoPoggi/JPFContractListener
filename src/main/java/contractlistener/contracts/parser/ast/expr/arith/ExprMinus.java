package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;

public class ExprMinus extends ArithmeticBinaryExpr {
    public ExprMinus(Operand o1, Operand o2) {
        super(o1, o2);
    }

    @Override
    Integer integerOperation(int n1, int n2) {
        return new Integer(n1 - n2);
    }

    @Override
    Double floatingPointOperation(double d1, double d2) {
        return new Double(d1 - d2);
    }

    @Override
    String operatorString() {
        return "-";
    }
}
