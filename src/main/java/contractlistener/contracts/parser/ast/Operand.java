package contractlistener.contracts.parser.ast;

import contractlistener.variables.VariablesRegistry;
import gov.nasa.jpf.vm.ElementInfo;

public interface Operand {
    Object getValue(VariablesRegistry registry);
    void saveOldValue(VariablesRegistry registry);

    default boolean isNumberValue(VariablesRegistry registry) {
        Object value = getValue(registry);

        if (value == null)
            return false;

        return value instanceof Number || value instanceof Character
                || ((ElementInfo) value).getFieldValueObject("value") instanceof Number
                || ((ElementInfo) value).getFieldValueObject("value") instanceof Character;
    }

    default Number getNumberValue(VariablesRegistry registry) {
        Object value = getValue(registry);

        if (value instanceof Number)
            return (Number) value;

        if (value instanceof Character)
            return (int) ((Character) value).charValue();

        if (((ElementInfo) value).getFieldValueObject("value") instanceof Number)
            return (Number) ((ElementInfo) value).getFieldValueObject("value");

        if (((ElementInfo) value).getFieldValueObject("value") instanceof Character)
            return (int) ((Character) ((ElementInfo) value).getFieldValueObject("value")).charValue();

        throw new IllegalArgumentException(this.toString() + " is not a number. " +
                "Value: " + value.toString());
    }

    default boolean isCharValue(VariablesRegistry registry) {
        Object value = getValue(registry);

        if (value == null)
            return false;

        return value instanceof Character ||
                ((ElementInfo) value).getFieldValueObject("value") instanceof Character;
    }

    default Character getCharValue(VariablesRegistry registry) {
        Object value = getValue(registry);

        if (value instanceof Character)
            return (Character) value;

        if (((ElementInfo) value).getFieldValueObject("value") instanceof Character)
            return (Character) ((ElementInfo) value).getFieldValueObject("value");

        throw new IllegalArgumentException(this.toString() + " is not a character. " +
                "Value: " + (value == null ? "null" : value.toString()));
    }
}
