package contractlistener.contracts.parser.ast;

import contractlistener.variables.VariablesRegistry;

public class Variable implements Operand {
    public final static String RESULT_NAME = "\\result";

    private String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        return registry.getValue(name);
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        // only used for old
    }

    @Override
    public String toString() {
        return name;
    }
}
