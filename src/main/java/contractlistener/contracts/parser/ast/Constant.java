package contractlistener.contracts.parser.ast;

import contractlistener.variables.VariablesRegistry;

public class Constant implements Operand {
    public static final Constant NULL = new Constant(null);
    public static final Constant TRUE = new Constant(true);
    public static final Constant FALSE = new Constant(false);

    private final Object value;

    public Constant(Object value) {
        this.value = value;
    }

    @Override
    public Object getValue(VariablesRegistry registry) {
        return this.value;
    }

    @Override
    public void saveOldValue(VariablesRegistry registry) {
        // only used for old
    }

    @Override
    public String toString() {
        if (value == null)
            return "null";

        if (value instanceof String)
            return "\"" + value + "\"";

        return value.toString();
    }
}
