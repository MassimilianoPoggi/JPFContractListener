package contractlistener.contracts.composition;

import contractlistener.contracts.Contract;
import contractlistener.contracts.EmptyContract;
import contractlistener.variables.VariablesRegistry;

public class ContractOr implements Contract {
    private Contract c1;
    private Contract c2;

    public ContractOr(Contract c1, Contract c2) {
        this.c1 = c1;
        this.c2 = c2;
    }

    @Override
    public boolean isSatisfied(VariablesRegistry registry) {
        return c1.isSatisfied(registry) || c2.isSatisfied(registry);
    }

    @Override
    public void saveOldValues(VariablesRegistry registry) {
        c1.saveOldValues(registry);
        c2.saveOldValues(registry);
    }

    @Override
    public String toString() {
        return "(" + c1.toString() + " || " + c2.toString() + ")";
    }
}
