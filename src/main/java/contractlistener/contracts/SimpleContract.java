package contractlistener.contracts;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;

public class SimpleContract implements Contract {
    private Operand operand;

    public SimpleContract(Operand operand) {
        this.operand = operand;
    }

    @Override
    public boolean isSatisfied(VariablesRegistry registry) {
        Object result = operand.getValue(registry);

        if (!(result instanceof Boolean))
            throw new IllegalArgumentException("Contract is not a boolean expression: " + this.toString());

        return (Boolean) result;
    }

    @Override
    public void saveOldValues(VariablesRegistry registry) {
        operand.saveOldValue(registry);
    }

    @Override
    public String toString() {
        return operand.toString();
    }
}
