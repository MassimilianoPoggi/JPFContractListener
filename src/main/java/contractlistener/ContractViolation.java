package contractlistener;

import contractlistener.contracts.Contract;
import contractlistener.tools.MethodCaller;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ContractViolation {
    enum Type {
        PRECONDITION("Requires"),
        POSTCONDITION("Ensures"),
        INVARIANT("Invariant");

        private String name;

        Type(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private Contract contract;
    private ContractViolation.Type type;
    private MethodInfo methodInfo;
    private List<String> arguments;

    public ContractViolation(Contract contract, ContractViolation.Type type, MethodInfo methodInfo,
                             ThreadInfo currentThread) {
        this.contract = contract;
        this.methodInfo = methodInfo;
        this.type = type;
        this.arguments = new ArrayList<>();

        for (Object o : currentThread.getTopFrame().getArgumentValues(currentThread)) {
            arguments.add(getValueAsString(currentThread, o));
        }
    }

    private String getValueAsString(ThreadInfo currentThread, Object o) {
        if (o == null)
            return "null";

        if (o instanceof Number || o instanceof Boolean)
            return o.toString();

        ElementInfo elementInfo = (ElementInfo) o;

        if (elementInfo.isStringObject())
            return "\"" + elementInfo.asString() + "\"";

        MethodCaller caller = new MethodCaller(currentThread, "toString", null);
        ElementInfo toStringResult = (ElementInfo) caller.invoke(elementInfo);

        return toStringResult.asString();
    }

    private String getParameterValues() {
        return arguments.stream()
                .collect(Collectors.joining(", "));
    }

    private String getMethodName() {
        String fullName = methodInfo.getFullName();
        return fullName.substring(0, fullName.indexOf("("));
    }

    @Override
    public String toString() {
        return "Call " + getMethodName()
                + "(" + getParameterValues() + ")"
                + " violated " + type.toString()
                + "(" + contract.toString() + ")";
    }
}
