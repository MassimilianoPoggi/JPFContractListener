package contractlistener;

import contractlistener.contracts.inheritance.EiffelInheritanceStrategy;
import contractlistener.contracts.inheritance.InheritanceCompositionStrategy;
import contractlistener.contracts.inheritance.JMLInheritanceStrategy;
import gov.nasa.jpf.Config;

public class ConfigurationParser {
    private Config config;

    public ConfigurationParser(Config config) {
        this.config = config;
    }

    public InheritanceCompositionStrategy getCompositionStrategy() {
        String strategy = config.getString("contract.inheritance", "jml");

        switch(strategy.toLowerCase()) {
            case "jml":
                return new JMLInheritanceStrategy();
            case "eiffel":
                return new EiffelInheritanceStrategy();
            default:
                throw new IllegalArgumentException("Unrecognized inheritance strategy.");
        }
    }
}
