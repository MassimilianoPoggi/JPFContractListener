package contractlistener;

import contractlistener.contracts.Contract;
import contractlistener.variables.*;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.PropertyListenerAdapter;
import gov.nasa.jpf.jvm.bytecode.JVMReturnInstruction;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.*;
import gov.nasa.jpf.vm.bytecode.InvokeInstruction;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ContractListener extends PropertyListenerAdapter {

    // initialization

    private ContractLoader contractLoader;
    private List<ContractViolation> contractViolations;

    public ContractListener(Config config) {
        ConfigurationParser configParser = new ConfigurationParser(config);

        contractLoader = new ContractLoader(configParser.getCompositionStrategy());
        contractViolations = new ArrayList<>();
    }

    // contract checking

    // postconditions + invariants
    @Override
    public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction instructionToExecute) {
        if (instructionToExecute instanceof JVMReturnInstruction) {

            // retrieve old values
            VariablesRegistry registry = currentThread.getTopFrame().getFrameAttr(VariablesRegistry.class);

            // postconditions
            StackFrame frame = currentThread.getTopFrame();
            MethodInfo method = frame.getMethodInfo();

            Contract postconditionsContract = contractLoader.getPostconditionsContract(method);

            VariablesLookupStrategy postconditionsLookupStrategy =
                    new PostconditionsVariablesLookupStrategy(currentThread, (JVMReturnInstruction) instructionToExecute);
            registry.setLookupStrategy(postconditionsLookupStrategy);

            testContract(postconditionsContract, ContractViolation.Type.POSTCONDITION, registry, method, currentThread);

            // invariants
            ClassInfo classInfo = method.getClassInfo();

            Contract invariantsContract;
            if (method.isStatic())
                invariantsContract = contractLoader.getStaticInvariantsContract(classInfo);
            else
                invariantsContract = contractLoader.getInstanceInvariantsContract(classInfo);

            testContract(invariantsContract, ContractViolation.Type.INVARIANT, registry, method, currentThread);
        }
    }

    // preconditions + remember values for postconditions
    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction instructionToExecute,
                                    Instruction executedInstruction) {
        if (executedInstruction instanceof InvokeInstruction) {

            // preconditions
            InvokeInstruction call = (InvokeInstruction) executedInstruction;
            MethodInfo method = call.getInvokedMethod();

            Contract preconditionsContract = contractLoader.getPreconditionsContract(method);

            VariablesRegistry variablesRegistry = new VariablesRegistry(
                    new PreconditionsVariablesLookupStrategy(currentThread));

            testContract(preconditionsContract, ContractViolation.Type.PRECONDITION,
                    variablesRegistry, method, currentThread);

            // remember old values
            Contract postconditionsContract = contractLoader.getPostconditionsContract(method);
            postconditionsContract.saveOldValues(variablesRegistry);
            currentThread.getTopFrame().addFrameAttr(variablesRegistry);
        }
    }

    private void testContract(Contract contract, ContractViolation.Type type,
                              VariablesRegistry variablesRegistry, MethodInfo method, ThreadInfo currentThread) {
        if (!contract.isSatisfied(variablesRegistry)) {
            ContractViolation violation = new ContractViolation(contract, type, method, currentThread);
            contractViolations.add(violation);
        }
    }

    // notify property violations

    @Override
    public boolean check(Search search, VM vm) {
        return contractViolations.isEmpty();
    }

    @Override
    public void reset() {
        contractViolations.clear();
    }

    @Override
    public String getErrorMessage() {
        return "Violations found:\n" +
                contractViolations.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining("\n"));
    }
}