grammar ContractSpec;

@header {
  package contractlistener.contracts.parser;

  import contractlistener.contracts.Contract;

  import contractlistener.contracts.parser.ast.*;
  import contractlistener.contracts.parser.ast.expr.arith.*;
  import contractlistener.contracts.parser.ast.expr.bool.logic.*;
  import contractlistener.contracts.parser.ast.expr.bool.comparators.*;
  import contractlistener.contracts.parser.ast.expr.lang.*;
}

options {
  language=Java;
}

/***************************** rules **************************************************************/
booleanExpr returns [Operand o]
    :
    o1=exprAnd                       { $o = $o1.o; }
    ('||' o2=exprAnd                 { $o = new ExprOr($o,$o2.o); }
    )*
    ;

exprAnd returns [Operand o]
    :
    o1=booleanAtom                   { $o = $o1.o; }
    ('&&' o2=booleanAtom             { $o = new ExprAnd($o,$o2.o); }
    )*
  ;

booleanAtom returns [Operand o]
    : 
    comparator                       { $o = $comparator.o; }
    | '!' booleanAtom                { $o = new ExprNot($booleanAtom.o); }
    ;
  
comparator returns [Operand o]
    :
    e1=arithExpr                     { $o = $e1.o; }
    (
    '==' e2=arithExpr                { $o = new ComparatorEQ($o, $e2.o); }
    | '!=' e3=arithExpr              { $o = new ComparatorNE($o, $e3.o); }
    | '>' e4=arithExpr               { $o = new ComparatorGT($o, $e4.o); }
    | '>=' e5=arithExpr              { $o = new ComparatorGE($o, $e5.o); }
    | '<' e6=arithExpr               { $o = new ComparatorLT($o, $e6.o); }
    | '<=' e7=arithExpr              { $o = new ComparatorLE($o, $e7.o); }
    )?
    ;

arithExpr returns [Operand o]
    :
    o1=mult                          { $o = $o1.o; }
    ( '+' o2=mult                    { $o = new ExprPlus($o, $o2.o); }
    | '-' o3=mult                    { $o = new ExprMinus($o, $o3.o); }
    )*
    ;
    
mult returns [Operand o]
    :   
    o1=unary                         { $o = $o1.o; }
    ( '*' o2=unary                   { $o = new ExprMult($o, $o2.o); }
    | '/' o3=unary                   { $o = new ExprDiv($o, $o3.o); }
    )*
    ;

unary returns [Operand o]
    :
    o1=fun                           { $o = $o1.o; }
    | '-' o2=unary                   { $o = new ExprUnaryMinus($o2.o); }
    | '+' o3=unary                   { $o = new ExprUnaryPlus($o3.o); }
    ;

fun returns [Operand o]
    :
    o1=dot                           { $o = $o1.o; }
    ( '.' ID '('                     { $o = new MethodCall($o, $ID.text); }
        ( o2=booleanExpr             { ((MethodCall)$o).addArgument($o2.o); }
            ( ',' o3=booleanExpr     { ((MethodCall)$o).addArgument($o3.o); }
            )*
        )?
      ')'
    )*
    ;

dot returns [Operand o]:
    o1=atom                          { $o = $o1.o; }
    ( '.' o2=var                     { $o = new ExprDot($o, $o2.o); }
    )*
    ;

atom returns [Operand o]:
    'null'                           { $o = Constant.NULL; }
    | 'true'                         { $o = Constant.TRUE; }
    | 'false'                        { $o = Constant.FALSE; }
    | '\\result'                     { $o = new Variable(Variable.RESULT_NAME); }
    | numOrVar                       { $o = $numOrVar.o; }
    | STRING                         { $o = new Constant($STRING.text); }
    | '\\old' '(' e1=booleanExpr ')' { $o = new ExprOld($e1.o); }
    | '(' e2=booleanExpr ')'         { $o = $e2.o; }
    ;

numOrVar returns [Operand o]:
    var                              { $o = $var.o; }
    | INT                            { $o = new Constant(new Integer($INT.text)); }
    | REAL                           { $o = new Constant(new Double($REAL.text)); }
    ;

var returns [Operand o]:
    ID                               { $o = new Variable($ID.text); }
    ;

  
/***************************** lexer **************************************************************/

ID :  ('a'..'z'|'A'..'Z'|'$'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'$'|'_')*;

fragment
SIGN: ('-'|'+');

fragment
NUM : ('0'..'9')+;

INT :  SIGN? NUM;

REAL: SIGN? NUM? '.' NUM ('e' SIGN? NUM)?;


fragment
ESCAPE
  :   '\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')
  ;

STRING
  :  '\'' ( ESCAPE | ~('\\'|'\'') )* '\'' { String s = getText(); setText(s.substring(1, s.length() - 1)); }
  ;
       
WS  : (' '|'\t')+  { skip(); };