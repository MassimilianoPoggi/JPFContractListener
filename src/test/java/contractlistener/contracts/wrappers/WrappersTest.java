package contractlistener.contracts.wrappers;

import contractlistener.contracts.Contract;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

@RunWith(Parameterized.class)
public class WrappersTest {
    private Contract wrappee;
    private Contract wrapper;

    // name is only used for display
    public WrappersTest(String name, Contract wrapper, Contract wrappee) {
        this.wrapper = wrapper;
        this.wrappee = wrappee;

        reset(wrappee);
    }

    @Test
    public void testSaveOldValues() {
        wrapper.saveOldValues(null);
        verify(wrappee).saveOldValues(null);
    }

    @Test
    public void testIsSatisfied() {
        wrapper.isSatisfied(null);
        verify(wrappee).isSatisfied(null);
    }

    @Parameterized.Parameters(name="{0}")
    public static Collection<Object[]> wrappers() {
        Contract c = mock(Contract.class);
        return Arrays.asList(new Object[][] {
                {"Preconditions", new PreconditionsContract(c), c},
                {"Postconditions", new PostconditionsContract(c), c},
                {"InstanceInvariants", new InstanceInvariantsContract(c), c},
                {"StaticInvariants", new StaticInvariantsContract(c), c}
        });
    }
}
