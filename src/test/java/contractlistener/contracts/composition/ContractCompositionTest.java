package contractlistener.contracts.composition;

import contractlistener.contracts.Contract;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class ContractCompositionTest {
    Contract c1 = mock(Contract.class);
    Contract c2 = mock(Contract.class);

    public ContractCompositionTest(boolean c1Satisfied, boolean c2Satisfied) {
        when(c1.isSatisfied(null)).thenReturn(c1Satisfied);
        when(c2.isSatisfied(null)).thenReturn(c2Satisfied);
    }

    @Test
    public void testAndComposition() {
        Contract c = new ContractAnd(c1, c2);

        assertEquals(c1.isSatisfied(null) && c2.isSatisfied(null),
                c.isSatisfied(null));
    }

    @Test
    public void testAndSavesValues() {
        Contract c = new ContractAnd(c1, c2);

        reset(c1, c2);

        c.saveOldValues(null);

        verify(c1).saveOldValues(null);
        verify(c2).saveOldValues(null);
    }

    @Test
    public void testOrComposition() {
        Contract c = new ContractOr(c1, c2);

        assertEquals(c1.isSatisfied(null) || c2.isSatisfied(null),
                c.isSatisfied(null));
    }

    @Test
    public void testOrSavesValues() {
        Contract c = new ContractOr(c1, c2);

        reset(c1, c2);

        c.saveOldValues(null);

        verify(c1).saveOldValues(null);
        verify(c2).saveOldValues(null);
    }

    @Parameterized.Parameters(name = "{0}, {1}")
    public static Collection<Object[]> truthTable() {
        return Arrays.asList(new Object[][] {
                {true, true},
                {true, false},
                {false, true},
                {false, false}
        });
    }
}
