package contractlistener.contracts.inheritance;

import contractlistener.contracts.Contract;
import contractlistener.contracts.inheritance.InheritanceCompositionStrategy;
import contractlistener.contracts.inheritance.JMLInheritanceStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class EiffelInheritanceTest {
    Contract c1 = mock(Contract.class);
    Contract c2 = mock(Contract.class);

    public EiffelInheritanceTest(boolean c1Satisfied, boolean c2Satisfied) {
        when(c1.isSatisfied(null)).thenReturn(c1Satisfied);
        when(c2.isSatisfied(null)).thenReturn(c2Satisfied);
    }

    @Test
    public void testPreconditionsComposition() {
        InheritanceCompositionStrategy strategy = new EiffelInheritanceStrategy();

        assertEquals(c1.isSatisfied(null) || c2.isSatisfied(null),
                strategy.composePreconditions(c1, c2).isSatisfied(null));
    }

    @Test
    public void testPostconditionsComposition() {
        InheritanceCompositionStrategy strategy = new EiffelInheritanceStrategy();

        assertEquals(c1.isSatisfied(null) && c2.isSatisfied(null),
                strategy.composePostconditions(c1, c2).isSatisfied(null));
    }

    @Test
    public void testInvariantsComposition() {
        InheritanceCompositionStrategy strategy = new EiffelInheritanceStrategy();

        assertEquals(c1.isSatisfied(null) && c2.isSatisfied(null),
                strategy.composeInvariants(c1, c2).isSatisfied(null));
    }

    @Parameterized.Parameters(name = "{0}, {1}")
    public static Collection<Object[]> truthTable() {
        return Arrays.asList(new Object[][] {
                {true, true},
                {true, false},
                {false, true},
                {false, false}
        });
    }
}

