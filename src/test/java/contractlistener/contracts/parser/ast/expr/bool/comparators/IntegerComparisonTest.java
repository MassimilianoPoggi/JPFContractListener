package contractlistener.contracts.parser.ast.expr.bool.comparators;

import contractlistener.contracts.parser.ast.Operand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class IntegerComparisonTest {
    private Operand o1 = mock(Operand.class);
    private Operand o2 = mock(Operand.class);

    public IntegerComparisonTest(Number val1, Number val2) {
        when(o1.getNumberValue(null)).thenReturn(val1);
        when(o2.getNumberValue(null)).thenReturn(val2);

        when(o1.getValue(null)).thenReturn(val1);
        when(o2.getValue(null)).thenReturn(val2);
    }

    @Test
    public void testGT() {
        Operand o = new ComparatorGT(o1, o2);

        assertEquals(o1.getNumberValue(null).intValue() > o2.getNumberValue(null).intValue(),
                o.getValue(null));
    }

    @Test
    public void testGE() {
        Operand o = new ComparatorGE(o1, o2);

        assertEquals(o1.getNumberValue(null).intValue() >= o2.getNumberValue(null).intValue(),
                o.getValue(null));
    }

    @Test
    public void testLT() {
        Operand o = new ComparatorLT(o1, o2);

        assertEquals(o1.getNumberValue(null).intValue() < o2.getNumberValue(null).intValue(),
                o.getValue(null));
    }

    @Test
    public void testLE() {
        Operand o = new ComparatorLE(o1, o2);

        assertEquals(o1.getNumberValue(null).intValue() <= o2.getNumberValue(null).intValue(),
                o.getValue(null));
    }

    @Test
    public void testEQ() {
        Operand o = new ComparatorEQ(o1, o2);

        assertEquals(o1.getNumberValue(null).intValue() == o2.getNumberValue(null).intValue(),
                o.getValue(null));
    }

    @Test
    public void testNE() {
        Operand o = new ComparatorNE(o1, o2);

        assertEquals(o1.getNumberValue(null).doubleValue() != o2.getNumberValue(null).doubleValue(),
                o.getValue(null));
    }

    @Parameterized.Parameters(name = "{0}, {1}")
    public static Collection<Object[]> operands() {
        return Arrays.asList(new Object[][] {
                {1, 1},
                {3, 1},
                {2, 5},
        });
    }
}