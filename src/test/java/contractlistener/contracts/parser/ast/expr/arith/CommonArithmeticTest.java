package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

@RunWith(Parameterized.class)
public class CommonArithmeticTest {
    private Operand o1;
    private Operand o2;
    private Operand o;

    // name is only used to print test name
    public CommonArithmeticTest(String name, Operand o, Operand o1, Operand o2) {
        this.o = o;
        this.o1 = o1;
        this.o2 = o2;

        reset(o1, o2);
    }

    @Test
    public void testSavesValues() {
        o.saveOldValue(null);

        verify(o1).saveOldValue(null);
        verify(o2).saveOldValue(null);
    }

    @Parameterized.Parameters(name="{0}")
    public static Collection<Object[]> operators() {
        Operand o1 = mock(Operand.class);
        Operand o2 = mock(Operand.class);

        return Arrays.asList(new Object[][] {
                {"Plus", new ExprPlus(o1, o2), o1, o2},
                {"Minus", new ExprMinus(o1, o2), o1, o2},
                {"Mult", new ExprMult(o1, o2), o1, o2},
                {"Div", new ExprDiv(o1, o2), o1, o2}
        });
    }
}
