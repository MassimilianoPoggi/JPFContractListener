package contractlistener.contracts.parser.ast.expr.arith;

import contractlistener.contracts.parser.ast.Operand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class FloatingPointArithmeticTest {
    private Operand o1 = mock(Operand.class);
    private Operand o2 = mock(Operand.class);

    public FloatingPointArithmeticTest(Number val1, Number val2) {
        when(o1.getNumberValue(null)).thenReturn(val1);
        when(o2.getNumberValue(null)).thenReturn(val2);
    }

    @Test
    public void testAdd() {
        Operand o = new ExprPlus(o1, o2);

        assertEquals(o1.getNumberValue(null).doubleValue() + o2.getNumberValue(null).doubleValue(),
                o.getNumberValue(null));
    }

    @Test
    public void testMul() {
        Operand o = new ExprMult(o1, o2);

        assertEquals(o1.getNumberValue(null).doubleValue() * o2.getNumberValue(null).doubleValue(),
                o.getNumberValue(null));
    }

    @Test
    public void testDiv() {
        Operand o = new ExprDiv(o1, o2);

        assertEquals(o1.getNumberValue(null).doubleValue() / o2.getNumberValue(null).doubleValue(),
                o.getNumberValue(null));
    }

    @Test
    public void testSub() {
        Operand o = new ExprMinus(o1, o2);

        assertEquals(o1.getNumberValue(null).doubleValue() - o2.getNumberValue(null).doubleValue(),
                o.getNumberValue(null));
    }

    @Parameterized.Parameters(name = "{0}, {1}")
    public static Collection<Object[]> operands() {
        return Arrays.asList(new Object[][] {
                {1.0, 1.0},
                {2.3, -3.4},
                {-5.4, 2.3},
                {-2.0, 2.0}
        });
    }
}
