package contractlistener.contracts.parser.ast.expr.lang;

import contractlistener.contracts.parser.ast.Operand;
import contractlistener.variables.VariablesRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ExprOldTest {
    private VariablesRegistry registry = mock(VariablesRegistry.class);
    private Operand o1 = mock(Operand.class);

    @Test
    public void testSavesField() {
        Object oValue = new Object();
        Operand o = new ExprOld(o1);

        when(o1.getValue(registry)).thenReturn(oValue);

        o.saveOldValue(registry);

        verify(registry).saveValue(o, oValue);
    }

    @Test
    public void testRetrievesField() {
        Object oValue = new Object();
        Operand o = new ExprOld(o1);

        when(registry.containsSavedValue(o)).thenReturn(true);
        when(registry.getValue(o)).thenReturn(oValue);

        assertEquals(oValue, o.getValue(registry));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionIfNotSaved() {
        Operand o = new ExprOld(o1);

        when(registry.containsSavedValue(o)).thenReturn(false);;

        o.getValue(registry);
    }
}
