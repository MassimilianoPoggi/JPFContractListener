package contractlistener.contracts.parser.ast.expr.bool.logic;

import contractlistener.contracts.parser.ast.Operand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(Parameterized.class)
public class BooleanLogicTest {
    Operand o1 = mock(Operand.class);
    Operand o2 = mock(Operand.class);

    public BooleanLogicTest(boolean o1Value, boolean o2Value) {
        when(o1.getValue(null)).thenReturn(o1Value);
        when(o2.getValue(null)).thenReturn(o2Value);
    }

    @Test
    public void testAndValue() {
        Operand o = new ExprAnd(o1, o2);

        assertEquals((Boolean) o1.getValue(null) && (Boolean) o2.getValue(null),
                o.getValue(null));
    }

    @Test
    public void testAndSavesValues() {
        Operand o = new ExprAnd(o1, o2);

        reset(o1, o2);

        o.saveOldValue(null);

        verify(o1).saveOldValue(null);
        verify(o2).saveOldValue(null);
    }

    @Test
    public void testOrValue() {
        Operand o = new ExprOr(o1, o2);

        assertEquals((Boolean) o1.getValue(null) || (Boolean) o2.getValue(null),
                o.getValue(null));
    }

    @Test
    public void testOrSavesValues() {
        Operand o = new ExprOr(o1, o2);

        reset(o1, o2);

        o.saveOldValue(null);

        verify(o1).saveOldValue(null);
        verify(o2).saveOldValue(null);
    }

    @Test
    public void testNotValue() {
        Operand o = new ExprNot(o1);

        assertEquals(! (Boolean) o1.getValue(null), o.getValue(null));
    }


    @Test
    public void testNotSavesValue() {
        Operand o = new ExprNot(o1);

        reset(o1);

        o.saveOldValue(null);

        verify(o1).saveOldValue(null);
    }

    @Parameterized.Parameters(name = "{0}, {1}")
    public static Collection<Object[]> truthTable() {
        return Arrays.asList(new Object[][] {
                {true, true},
                {true, false},
                {false, true},
                {false, false}
        });
    }
}
