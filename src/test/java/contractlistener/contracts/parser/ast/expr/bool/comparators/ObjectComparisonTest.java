package contractlistener.contracts.parser.ast.expr.bool.comparators;

import contractlistener.contracts.parser.ast.Operand;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(Parameterized.class)
public class ObjectComparisonTest {
    private Object o1Value;
    private Object o2Value;
    private Operand o1 = mock(Operand.class);
    private Operand o2 = mock(Operand.class);

    //name is only used for display
    public ObjectComparisonTest(String name, Object o1Value, Object o2Value) {
        when(o1.getValue(null)).thenReturn(o1Value);
        when(o2.getValue(null)).thenReturn(o2Value);
        this.o1Value = o1Value;
        this.o2Value = o2Value;
    }

    @Test
    public void testEQ() {
        Operand o = new ComparatorEQ(o1, o2);

        assertEquals(o1Value.equals(o2Value), o.getValue(null));
    }

    @Test
    public void testNE() {
        Operand o = new ComparatorNE(o1, o2);

        assertEquals(!o1Value.equals(o2Value), o.getValue(null));
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> objects() {
        Object o1 = new Object();
        Object o2 = new Object();
        return Arrays.asList(new Object[][] {
                {"Equal objects", o1, o1 },
                {"Different objects", o1, o2 },
                {"Equal booleans", true, true},
                {"Different booleans", true, false}
        });
    }
}
