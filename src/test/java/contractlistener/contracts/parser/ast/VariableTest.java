package contractlistener.contracts.parser.ast;

import contractlistener.variables.VariablesRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VariableTest {
    private VariablesRegistry registry = mock(VariablesRegistry.class);

    @Test
    public void testRetrieveValue() {
        Object vValue = new Object();
        Variable v = new Variable("varName");

        when(registry.getValue("varName")).thenReturn(vValue);

        assertEquals(vValue, v.getValue(registry));
    }
}
