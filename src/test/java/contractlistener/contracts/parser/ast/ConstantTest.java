package contractlistener.contracts.parser.ast;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConstantTest {
    @Test
    public void testRetrieveValue() {
        Object value = new Object();
        Constant c = new Constant(value);

        assertEquals(value, c.getValue(null));
    }
}
