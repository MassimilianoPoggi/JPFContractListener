package contractlistener;

import contractlistener.annotations.Ensures;
import contractlistener.annotations.InstanceInvariant;
import contractlistener.annotations.Requires;
import gov.nasa.jpf.util.TypeRef;
import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Test;

public class AccountTest extends TestJPF {
    @InstanceInvariant({ "this.getCreditLimit() >= 0",
            "this.getBalance() >= -this.getCreditLimit()" })
    private class Account {
        private int balance;
        private int creditLimit;

        @Requires({ "balance >= 0", "creditLimit >= balance" })
        public Account(int balance, int creditLimit) {
            this.balance = balance;
            this.creditLimit = creditLimit;
        }

        @Ensures({ "\\result == this.balance" })
        public int getBalance() {
            return this.balance;
        }

        @Ensures({ "\\result == this.creditLimit" })
        public int getCreditLimit() {
            return this.creditLimit;
        }

        @Ensures({ "\\result == this.getBalance() + this.getCreditLimit()"})
        public int getAvailableAmount() {
            return this.getBalance() + this.getCreditLimit();
        }

        @Requires({ "creditLimit <= Math.max(-balance, 0)"})
        @Ensures({ "this.getCreditLimit() == creditLimit" })
        public void setCreditLimit(int creditLimit) {
            this.creditLimit = creditLimit;
        }

        @Requires({ "amount > 0" })
        @Ensures({ "this.getBalance() == \\old(this.getBalance()) + amount" })
        public synchronized void deposit(int amount) {
            this.balance += amount;
        }

        @Requires({ "amount > 0 && amount <= this.getAvailableAmount()" })
        @Ensures({ "this.getBalance() == \\old(this.getBalance()) - amount" })
        public synchronized void withdraw(int amount) {
            this.balance -= amount;
        }

        @Requires({ "amount > 0 && amount <= this.getAvailableAmount()",
                "other != null && other != this" })
        @Ensures({ "this.getBalance() == \\old(this.getBalance()) - amount",
                "other.getBalance() == \\old(other.getBalance()) + amount" })
        public void transfer(int amount, Account other) {
            this.withdraw(amount);
            other.deposit(amount);
        }
    }

    TypeRef contractProperty = new TypeRef(ContractListener.class.getName());

    @Test
    public void testNegativeBalanceConstructor() {
        if (verifyPropertyViolation(contractProperty)) {
            new Account(-1, 0);
        }
    }

    @Test
    public void testLowCreditLimitConstructor() {
        if (verifyPropertyViolation(contractProperty)) {
            new Account(20, 10);
        }
    }

    @Test
    public void testWithdrawNegativeAmount() {
        if (verifyPropertyViolation(contractProperty)) {
            Account a = new Account(10, 20);
            a.withdraw(-5);
        }
    }

    @Test
    public void testWithdrawTooMuch() {
        if (verifyPropertyViolation(contractProperty)) {
            Account a = new Account(10, 20);
            a.withdraw(50);
        }
    }

    @Test
    public void testWithdrawValid() {
        if (verifyNoPropertyViolation()) {
            Account a = new Account(10, 20);
            a.withdraw(10);
            a.withdraw(10);
        }
    }

    @Test
    public void testDepositNegativeAmount() {
        if (verifyPropertyViolation(contractProperty)) {
            Account a = new Account(10, 20);
            a.deposit(-5);
        }
    }

    @Test
    public void testDepositValid() {
        if (verifyNoPropertyViolation()) {
            Account a = new Account(10, 20);
            a.deposit(10);
        }
    }

    @Test
    public void testSameAccountTransfer() {
        if (verifyPropertyViolation(contractProperty)) {
            Account a = new Account(10, 20);
            a.transfer(10, a);
        }
    }

    @Test
    public void testTransferValid() {
        if (verifyNoPropertyViolation()) {
            Account a = new Account(10, 20);
            Account b = new Account(10, 20);
            a.transfer(10, b);
        }
    }

    //TODO: uncomment after resolving issue #1 to test
    //@Test
    public void testNegativeCreditLimit() {
        if (verifyPropertyViolation(contractProperty)) {
            Account a = new Account(10, 20);
            a.setCreditLimit(-5);
        }
    }
}
