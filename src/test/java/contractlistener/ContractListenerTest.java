package contractlistener;

import contractlistener.annotations.Ensures;
import contractlistener.annotations.InstanceInvariant;
import contractlistener.annotations.Requires;
import contractlistener.annotations.StaticInvariant;
import gov.nasa.jpf.util.TypeRef;
import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Test;

public class ContractListenerTest extends TestJPF {
    public TypeRef contractListener = new TypeRef(ContractListener.class.getName());

    @Test
    public void testValidConstructor() {
        if (verifyNoPropertyViolation()) {
            new SystemUnderTest(1, "prova");
        }
    }

    @Test
    public void testNullConstant() {
        if (verifyPropertyViolation(contractListener)) {
            new SystemUnderTest(1, null);
        }
    }

    @Test
    public void testComparisonContract() {
        if (verifyPropertyViolation(contractListener)) {
            new SystemUnderTest(-1, "ok");
        }
    }

    @Test
    public void testStaticContract() {
        if (verifyNoPropertyViolation()) {
            SystemUnderTest.getC();
        }
    }

    @Test
    public void testInvalidStaticContract() {
        if (verifyPropertyViolation(contractListener)) {
            SystemUnderTest.getCWrong();
        }
    }

    @Test
    public void testMethodCallContract() {
        if (verifyNoPropertyViolation()) {
            SystemUnderTest sut = new SystemUnderTest(1, "prova");
            sut.setA(2);
        }
    }

    @Test
    public void testInvalidMethodCallContract() {
        if (verifyPropertyViolation(contractListener)) {
            SystemUnderTest sut = new SystemUnderTest(1, "prova");
            sut.setAWrong(4);
        }
    }

    @Test
    public void testInvalidInvariant() {
        if (verifyPropertyViolation(contractListener)) {
            SystemUnderTest sut = new SystemUnderTest(1, "prova");
            sut.setA(3);
            sut.setA(-1);
        }
    }
}

@InstanceInvariant("a > 0")
@StaticInvariant("c > 0")
class SystemUnderTest {
    static int c = 3;

    private Integer a;
    private String b;
    private SystemUnderTest2 d;

    @Requires({ "a > 0", "b != null" })
    @Ensures({ "this.a == a", "this.d.a < Math.PI" })
    SystemUnderTest(int a, String b) {
        this.a = a;
        this.b = b;
        this.d = new SystemUnderTest2(2, this);
    }

    @Ensures({ "\\result == SystemUnderTest.c" })
    public static int getC() {
        return c;
    }

    @Ensures({ "\\result == c" })
    public static int getCWrong() {
        return c - 1;
    }

    @Ensures({ "this.getA() == a", "SystemUnderTest.getC() == c" })
    public void setA(int a) {
        this.a = a;
    }

    @Ensures({ "this.getA() == a" })
    public void setAWrong(int a) {
        this.a = a - 1;
    }

    @Ensures({ "\\result == a" })
    public int getA() {
        return this.a;
    }

    @Override
    public String toString() {
        return "SystemUnderTest{" +
                "a=" + a +
                ", b='" + b + '\'' +
                ", d=" + d +
                '}';
    }
}

class SystemUnderTest2 {
    private int a;
    private SystemUnderTest b;

    SystemUnderTest2(int a, SystemUnderTest b) {
        this.a = a;
        this.b = b;
    }
}