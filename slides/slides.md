# JPF Internals

Repository di riferimento:  
https://gitlab.com/MassimilianoPoggi/JPFContractListener

---

### Introduzione
 
Perché JPF?
  - Testing esaustivo di parti critiche
  - Tracciamento esaustivo delle condizioni che portano ad un bug
  - Completo controllo sull’esecuzione


Perché estenderlo?
  - L’ecosistema non è molto sviluppato
  - Proprietà specifiche dell’applicazione sotto test

---

### Obiettivo finale

```java
    @InstanceInvariant({ "this.getCreditLimit() >= 0",
      "this.getBalance() >= -this.getCreditLimit()" })
    public class Account {        
        private int balance;
        private int creditLimit;
    
        // [..]
    
        @Ensures({ "\\result == this.balance" })
        public int getBalance() {
            return this.balance;
        }
        
        // [..]
    
        @Requires({ "amount > 0 && amount <= this.getAvailableAmount()",
                "other != null && other != this" })
        @Ensures({ "this.getBalance() == \\old(this.getBalance()) - amount",
                "other.getBalance() == \\old(other.getBalance()) + amount" })
        public void transfer(int amount, Account other) {
            this.withdraw(amount);
            other.deposit(amount);
        }
    }
```

---

### Design JPF  

![image](images/jpf-design.png)

---

### Modello dei dati  

Tutto è rappresentato come un intero.

  - Tipi primitivi    
      - Byte/Short/Int/Char -\> loro valore
      - Long -\> valore codificato su due campi
      - Float -\> valore codificato come intero
      - Double -\> valore codificato come intero su due campi
      - Boolean -\> intero (0 = False, resto True)

  - Tipi di riferimento (struttura ElementInfo)
      - Intero fa riferimento all’oggetto vero nella JVM
      - Valori dei campi codificati nella struttura Fields
      - Struttura Monitor per gestire i lock

### Modello dei meta-dati 

Simile per molti aspetti alle reflection di Java.

  - ClassInfo:
      - Ricerca di metodi
      - Ricerca dei campi (sia statici che di istanza)
      - Accesso alla gerarchia di superclassi/interfacce
      - Accesso alle annotazioni della classe
      - Accesso a classi interne/esterne
      - Accesso al namespace degli import

  - MethodInfo
      - Accesso alle annotazioni del metodo
      - Accesso alla signature
      - Accesso alle variabili locali
      - Accesso alle singole istruzioni
      - Source location

  - FieldInfo
      - Descrittore del campo (non contiene il valore)
      - Accesso alla signature

##### Utilizzo

Recupero dei valori dei campi:
```java
    private FieldInfo getStaticField(String key) {
        return classInfo.getStaticField(key);
    }

    private FieldInfo getInstanceField(String key) {
        if (thisElement != null)
            return classInfo.getInstanceField(key);

        return null;
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        FieldInfo staticField = getStaticField(key);
        if (staticField != null)
            return classInfo.getStaticFieldValueObject(key);

        FieldInfo instanceField = getInstanceField(key);
        if (instanceField != null)
            return thisElement.getFieldValueObject(key);

        // [..]
    }
```

---

### Modello di esecuzione

  - ThreadInfo
      - Accesso allo stato del thread
      - Accesso allo stack
      - Accesso ai lock in possesso del thread
      - Possibilità di eseguire metodi/istruzioni sul thread

  - StackFrame
      - Accesso al metodo in esecuzione
      - Accesso alle variabili locali
      - Accesso agli operandi
      - Accesso al program counter

##### Istruzioni  

![image](images/jpf-instructions.png)

##### Utilizzo  

Invocazione di metodi del sistema sotto test;
```java
     public Object invoke(Object target) {
        Memento<ThreadInfo> memento = executionThread.getMemento();

        MethodInfo method = getMethod(getClassInfo(target));

        StackFrame frame = method.createDirectCallStackFrame(executionThread, 0);
        if (!method.isStatic())
            frame.push(getObjectReference(target));

        for (int i = 0; i < arguments.size(); i++)
            pushArgumentValue(method, frame, i);

        executionThread.executeMethodHidden(frame);

        memento.restore(executionThread);

        return getResultObject(method, frame);
    }
```

---

### Sistema di attributi

![image](images/jpf-attributes.png)

##### Utilizzo

Salvataggio dei contratti
```java
    public Contract getPreconditionsContract(MethodInfo method) {
        if (method.hasAttr(PreconditionsContract.class))
            return method.getAttr(PreconditionsContract.class);

        Contract contract = new PreconditionsContract(loadContract(Requires.class, method));

        MethodInfo overriddenMethod = method.getOverriddenMethodInfo();
        if (overriddenMethod != null)
            contract = inheritanceStrategy.composePreconditions(contract, getPreconditionsContract(overriddenMethod));

        contract = new PreconditionsContract(contract);
        method.addAttr(contract);

        return contract;
    }
  
```

Salvataggio dei valori \\old

``` 
    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction instructionToExecute,
                                    Instruction executedInstruction) {
       // [..]
            Contract postconditionsContract = contractLoader.getPostconditionsContract(method);
            postconditionsContract.saveOldValues(variablesRegistry);
            currentThread.getTopFrame().addFrameAttr(variablesRegistry);
       // [..]
    }
  
```

---

### Model Java interface

##### Struttura

![image](images/jpf-layers.png)

##### Delegazione

![image](images/mji-functions.png)

##### Chiamata a funzione

![image](images/mji-call.png)

##### Traduzione

![image](images/mji-mangling.png)

##### Generazione automatica

![image](images/mji-genpeer.png)

##### Utilizzo

Gestione del boxing automatico e delle costanti:
``` 
    private int getObjectReference(Object o) {
        if (o instanceof ElementInfo)
            return ((ElementInfo) o).getObjectRef();

        MJIEnv env = executionThread.getEnv();

        if (o == null)
            return MJIEnv.NULL;
        if (o instanceof Integer)
            return env.newInteger((Integer) o);
        if (o instanceof Long)
            return env.newLong((Long) o);
        if (o instanceof Boolean)
            return env.newBoolean((Boolean) o);
        if (o instanceof Double)
            return env.newDouble((Double) o);
        if (o instanceof Float)
            return env.newFloat((Float) o);
        if (o instanceof Character)
            return env.newCharacter((Character) o);
        if (o instanceof String)
            return env.newString(o.toString());

        throw new IllegalArgumentException("Unexpected argument type: " + o.getClass());
    }
  
```

---

### Metterci le mani 
Se avete voglia ho aperto qualche issue sul repo per fare pratica.  

![image](images/contract-issues.png)
