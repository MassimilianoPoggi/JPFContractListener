\documentclass{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\tiny\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\title{Exploring JPF Internals}
\author{Massimiliano Poggi}
\date{February 26th, 2018}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Introduzione}
  Perché JPF?
  \begin{itemize}
    \item Testing esaustivo di parti critiche
    \item Tracciamento esaustivo delle condizioni che portano ad un bug
    \item Completo controllo sull'esecuzione
  \end{itemize}
  \bigskip
  \bigskip
  Perché estenderlo?
  \begin{itemize}
    \item L'ecosistema non è molto sviluppato
    \item Proprietà specifiche dell'applicazione sotto test
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Obiettivo finale}
  \begin{lstlisting}
    @InstanceInvariant({ "this.getCreditLimit() >= 0",
      "this.getBalance() >= -this.getCreditLimit()" })
    public class Account {        
        private int balance;
        private int creditLimit;
    
        // [..]
    
        @Ensures({ "\\result == this.balance" })
        public int getBalance() {
            return this.balance;
        }
        
        // [..]
    
        @Requires({ "amount > 0 && amount <= this.getAvailableAmount()",
                "other != null && other != this" })
        @Ensures({ "this.getBalance() == \\old(this.getBalance()) - amount",
                "other.getBalance() == \\old(other.getBalance()) + amount" })
        public void transfer(int amount, Account other) {
            this.withdraw(amount);
            other.deposit(amount);
        }
    }
  \end{lstlisting}
\end{frame}

\begin{frame}{Design JPF}
  \includegraphics[width=\textwidth]{images/jpf-design.png}
\end{frame}

\begin{frame}{Modello dei dati}
  Tutto è rappresentato come un intero.
  \begin{itemize}
    \item Tipi primitivi
    \begin{itemize}
      \item Byte/Short/Int/Char -> loro valore
      \item Long -> valore codificato su due campi
      \item Float -> valore codificato come intero
      \item Double -> valore codificato come intero su due campi
      \item Boolean -> intero (0 = False, resto True)
    \end{itemize}
    \item Tipi di riferimento (struttura ElementInfo)
    \begin{itemize}
      \item Intero fa riferimento all'oggetto vero nella JVM
      \item Valori dei campi codificati nella struttura Fields
      \item Struttura Monitor per gestire i lock
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Modello dei meta-dati}
  Simile per molti aspetti alle reflection di Java
  \bigskip
  \bigskip
  \begin{itemize}
    \item ClassInfo:
    \begin{itemize}
      \item Ricerca di metodi
      \item Ricerca dei campi (sia statici che di istanza)
      \item Accesso alla gerarchia di superclassi/interfacce
      \item Accesso alle annotazioni della classe
      \item Accesso a classi interne/esterne
      \item Accesso al namespace degli import
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Modello dei meta-dati}
  \begin{itemize}
    \item MethodInfo
    \begin{itemize}
        \item Accesso alle annotazioni del metodo
        \item Accesso alla signature
        \item Accesso alle variabili locali
        \item Accesso alle singole istruzioni
        \item Source location
    \end{itemize}
    \item FieldInfo
    \begin{itemize}
      \item Descrittore del campo (non contiene il valore)
      \item Accesso alla signature  
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Modello dei dati - utilizzo}
  \tiny{Retrieval dei valori dei campi}
  \begin{lstlisting}
    private FieldInfo getStaticField(String key) {
        return classInfo.getStaticField(key);
    }

    private FieldInfo getInstanceField(String key) {
        if (thisElement != null)
            return classInfo.getInstanceField(key);

        return null;
    }

    @Override
    public Object getValue(String key) throws IllegalArgumentException {
        FieldInfo staticField = getStaticField(key);
        if (staticField != null)
            return classInfo.getStaticFieldValueObject(key);

        FieldInfo instanceField = getInstanceField(key);
        if (instanceField != null)
            return thisElement.getFieldValueObject(key);

        // [..]
    }
  \end{lstlisting}
\end{frame}

\begin{frame}{Modello di esecuzione}
  \begin{itemize}
    \item ThreadInfo
    \begin{itemize}
      \item Accesso allo stato del thread
      \item Accesso allo stack
      \item Accesso ai lock in possesso del thread
      \item Possibilità di eseguire metodi/istruzioni sul thread
    \end{itemize}
    \item StackFrame
    \begin{itemize}
      \item Accesso al metodo in esecuzione
      \item Accesso alle variabili locali 
      \item Accesso agli operandi
      \item Accesso al program counter
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Modello di esecuzione}{Istruzioni}
  \centering{\includegraphics[width=0.9\textwidth]{images/jpf-instructions.png}}
\end{frame}

\begin{frame}[fragile]{Modello di esecuzione - utilizzo}
  \tiny{Invocazione dei metodi dell'applicazione}
  \begin{lstlisting}
     public Object invoke(Object target) {
        Memento<ThreadInfo> memento = executionThread.getMemento();

        MethodInfo method = getMethod(getClassInfo(target));

        StackFrame frame = method.createDirectCallStackFrame(executionThread, 0);
        if (!method.isStatic())
            frame.push(getObjectReference(target));

        for (int i = 0; i < arguments.size(); i++)
            pushArgumentValue(method, frame, i);

        executionThread.executeMethodHidden(frame);

        memento.restore(executionThread);

        return getResultObject(method, frame);
    }
  \end{lstlisting}
\end{frame}

\begin{frame}{Sistema di attributi}
  \centering{\includegraphics[width=0.9\textwidth]{images/jpf-attributes.png}}
\end{frame}

\begin{frame}[fragile]{Sistema di attributi - utilizzo}
  \tiny{Retrieval contratti:}
  \begin{lstlisting}
    public Contract getPreconditionsContract(MethodInfo method) {
        if (method.hasAttr(PreconditionsContract.class))
            return method.getAttr(PreconditionsContract.class);

        Contract contract = new PreconditionsContract(loadContract(Requires.class, method));

        MethodInfo overriddenMethod = method.getOverriddenMethodInfo();
        if (overriddenMethod != null)
            contract = inheritanceStrategy.composePreconditions(contract, getPreconditionsContract(overriddenMethod));

        contract = new PreconditionsContract(contract);
        method.addAttr(contract);

        return contract;
    }
  \end{lstlisting}
  \tiny{Salvataggio dei valori $\backslash$old:}
  \begin{lstlisting}
    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction instructionToExecute,
                                    Instruction executedInstruction) {
       // [..]
            Contract postconditionsContract = contractLoader.getPostconditionsContract(method);
            postconditionsContract.saveOldValues(variablesRegistry);
            currentThread.getTopFrame().addFrameAttr(variablesRegistry);
       // [..]
    }
  \end{lstlisting}
\end{frame}

\begin{frame}{Model Java interface}{Struttura}
  \centering{\includegraphics[width=0.8\textwidth]{images/jpf-layers.png}}
\end{frame}

\begin{frame}{Model Java interface}{Delegazione}
  \centering{\includegraphics[width=0.8\textwidth]{images/mji-functions.png}}
\end{frame}

\begin{frame}{Model Java interface}{Chiamata a funzione}
  \centering{\includegraphics[width=0.8\textwidth]{images/mji-call.png}}
\end{frame}

\begin{frame}{Model Java interface}{Traduzione}
  \centering{\includegraphics[width=0.8\textwidth]{images/mji-mangling.png}}
\end{frame}

\begin{frame}{Model Java interface}{Generazione automatica}
  \centering{\includegraphics[width=0.8\textwidth]{images/mji-genpeer.png}}
\end{frame}

\begin{frame}[fragile]{Model Java interface - utilizzo}
  \tiny{Gestione del boxing/di costanti}
  \begin{lstlisting}
    private int getObjectReference(Object o) {
        if (o instanceof ElementInfo)
            return ((ElementInfo) o).getObjectRef();

        MJIEnv env = executionThread.getEnv();

        if (o == null)
            return MJIEnv.NULL;
        if (o instanceof Integer)
            return env.newInteger((Integer) o);
        if (o instanceof Long)
            return env.newLong((Long) o);
        if (o instanceof Boolean)
            return env.newBoolean((Boolean) o);
        if (o instanceof Double)
            return env.newDouble((Double) o);
        if (o instanceof Float)
            return env.newFloat((Float) o);
        if (o instanceof Character)
            return env.newCharacter((Character) o);
        if (o instanceof String)
            return env.newString(o.toString());

        throw new IllegalArgumentException("Unexpected argument type: " + o.getClass());
    }
  \end{lstlisting}
\end{frame}

\begin{frame}{Metterci le mani}
  Se avete voglia ho aperto qualche issue sul repo per fare pratica.
  \bigskip
  \begin{figure}[b]
    \includegraphics[width=0.5\textwidth]{images/contract-issues.png}
  \end{figure}
\end{frame}

\end{document}
